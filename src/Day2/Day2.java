package Day2;

/**
 * Traverse a number pad to find a secret code by executing an extremely long set of instructions
 */
public class Day2 {
    String[] instructionList;

    Day2(String[] instrctionList) {
        this.instructionList = instrctionList;
    }

    String partOne() {
        String newCode = "";
        int currentNo = 5;

        for (String instuction : instructionList) {
            for (int i = 0; i < instuction.length(); i++) {
                char dirToMove = instuction.charAt(i);
                if (dirToMove == 'L') {
                    if (currentNo != 1 && currentNo != 4 && currentNo != 7)
                        currentNo--;
                } else if (dirToMove == 'R') {
                    if (currentNo != 3 && currentNo != 6 && currentNo != 9)
                        currentNo++;
                } else if (dirToMove == 'U') {
                    if (currentNo >= 4)
                        currentNo -= 3;
                } else if (dirToMove == 'D') {
                    if (currentNo <= 6)
                        currentNo += 3;
                }
            }
            newCode = newCode + currentNo;
            currentNo = 5;
        }
        return newCode;
    }

    String partTwo() {
        String newCode = "";
        char[][] directions = {
                {'X', 'X', '1', 'X', 'X'},
                {'X', '2', '3', '4', 'X'},
                {'5', '6', '7', '8', '9'},
                {'X', 'A', 'B', 'C', 'X'},
                {'X', 'X', 'D', 'X', 'X'}};
        int[] pos = new int[2];
        int[] newPos = new int[2];

        for (String instruction : instructionList) {
            pos[0] = 0;
            pos[1] = 2;
            newPos[0] = 0;
            newPos[1] = 2;
            for (int i = 0; i < instruction.length(); i++) {
                // Firstly find what the new location would be
                char dirToMove = instruction.charAt(i);
                if (dirToMove == 'L')
                    newPos[0] = newPos[0] - 1;
                else if (dirToMove == 'R')
                    newPos[0] = newPos[0] + 1;
                else if (dirToMove == 'U')
                    newPos[1] = newPos[1] - 1;
                else if (dirToMove == 'D')
                    newPos[1] = newPos[1] + 1;

                // Second, check not off board
                if (newPos[0] >= 0 && newPos[0] < 5 && newPos[1] >= 0 && newPos[1] < 5 &&
                        // And check at a legal position
                        directions[newPos[0]][newPos[1]] != 'X') {
                    pos[0] = newPos[0];
                    pos[1] = newPos[1];
                } else {
                    newPos[0] = pos[0];
                    newPos[1] = pos[1];
                }
            }
            // NOTE! Because 2x2 arrays actually go columns then rows, my code was traversing with rows and columns
            // swapped, hence need to swap the array indices to get the correct value
            newCode = newCode + directions[pos[1]][pos[0]];
        }
        return newCode;
    }

    public static void main(String[] args) {
        String input = "RRLUDDLDUDUDUDRDDDRDDRLUUUDRUDURURURLRDDULLLDRRRRULDDRDDURDLURLURRUULRURDDDDLDDRRLDUDUUDURURDLDRRURDLLLDLLRUDRLDDRUULLLLLRRLDUDLUUDRUULLRLLLRLUURDLDLLDDRULDLUURRURLUUURLLDDULRDULULRULDDLRDDUUDLRRURLLURURLDDLURRLUURRRRLDRDLDUDRUDDRULLDUDDLRRLUUUUUDDLLDRLURDDRLLUDULDRDDLLUURUUUURDRLRLLULUULULLRRDLULRUDURDLRLRDDDRULLUULRURULLLUDUURUUUURUULDURDRRRULRLULDLRRULULUUDDDRDURLLURLLDUUUUDULRDLRDUUDDLDUDRLLRLRRRLULUDDDURLRRURUDDDRDRDRLLRDRDLDDRRDRDLLRLLLRRULRDDURRDUDRURDLDULLRRLURLRLLDURRRLLDRRURRRUULDRLDUULRDLDLURUDLLDLLUUDDDUUUDRL\n" +
                "DLRRDRRDDRRDURLUDDDDDULDDLLDRLURDDDDDDRDDDRDDDLLRRULLLRUDULLDURULRRDLURURUDRUURDRLUURRUDRUULUURULULDDLLDDRLDUDDRDRDDUULDULDDLUDUDDUDLULLUDLLLLLRRRUURLUUUULRURULUDDULLLRLRDRUUULULRUUUULRDLLDLDRDRDRDRRUUURULDUUDLDRDRURRUDDRLDULDDRULRRRLRDDUUDRUDLDULDURRDUDDLULULLDULLLRRRDULLLRRURDUURULDRDURRURRRRDLDRRUDDLLLDRDRDRURLUURURRUUURRUDLDDULDRDRRURDLUULDDUUUURLRUULRUURLUUUDLUDRLURUDLDLDLURUURLDURDDDDRURULLULLDRDLLRRLDLRRRDURDULLLDLRLDR\n" +
                "URURLLDRDLULULRDRRDDUUUDDRDUURULLULDRLUDLRUDDDLDRRLURLURUUDRLDUULDRDURRLLUDLDURRRRLURLDDRULRLDULDDRRLURDDRLUDDULUDULRLDULDLDUDRLLDDRRRDULLDLRRLDRLURLUULDDDDURULLDLLLDRRLRRLLRDDRDLDRURRUURLLDDDLRRRRRDLRRDRLDDDLULULRLUURULURUUDRULRLLRDLDULDRLLLDLRRRUDURLUURRUDURLDDDRDRURURRLRRLDDRURULDRUURRLULDLUDUULDLUULUDURRDDRLLLRLRRLUUURRDRUULLLRUUURLLDDRDRULDULURRDRURLRRLRDURRURRDLDUDRURUULULDDUDUULDRDURRRDLURRLRLDUDRDULLURLRRUDLUDRRRULRURDUDDDURLRULRRUDUUDDLLLURLLRLLDRDUURDDLUDLURDRRDLLRLURRUURRLDUUUUDUD\n" +
                "DRRDRRRLDDLDUDRDLRUUDRDUDRRDUDRDURRDDRLLURUUDRLRDDULLUULRUUDDRLDLRULDLRLDUDULUULLLRDLURDRDURURDUDUDDDRRLRRLLRULLLLRDRDLRRDDDLULDLLUUULRDURRULDDUDDDURRDRDRDRULRRRDRUDLLDDDRULRRLUDRDLDLDDDLRLRLRLDULRLLRLRDUUULLRRDLLRDULURRLDUDDULDDRLUDLULLRLDUDLULRDURLRULLRRDRDDLUULUUUULDRLLDRDLUDURRLLDURLLDDLLUULLDURULULDLUUDLRURRRULUDRLDRDURLDUDDULRDRRDDRLRRDDRUDRURULDRRLUURUDULDDDLRRRRDRRRLLURUURLRLULUULLRLRDLRRLLUULLDURDLULURDLRUUDUUURURUURDDRLULUUULRDRDRUUDDDRDRL\n" +
                "RLRUDDUUDDDDRRLRUUDLLDRUUUDRRDLDRLRLLDRLUDDURDLDUDRRUURULLRRLUULLUDRDRUDDULRLLUDLULRLRRUUDLDLRDDDRDDDUDLULDLRRLUDUDDRRRRDRDRUUDDURLRDLLDLDLRRDURULDRLRRURULRDDLLLRULLRUUUDLDUURDUUDDRRRDDRLDDRULRRRDRRLUDDDRUURRDRRDURDRUDRRDLUDDURRLUDUDLLRUURLRLLLDDURUDLDRLRLLDLLULLDRULUURLDDULDDRDDDURULLDRDDLURRDDRRRLDLRLRRLLDLLLRDUDDULRLUDDUULUDLDDDULULDLRDDLDLLLDUUDLRRLRDRRUUUURLDLRRLDULURLDRDURDDRURLDLDULURRRLRUDLDURDLLUDULDDU";
        String[] instructionList = input.split("\n");
        Day2 today = new Day2(instructionList);

        String answer1 = today.partOne();
        // Answer is 48584
        System.out.println("The code is " + answer1);

        String answer2 = today.partTwo();
        System.out.println("The code is " + answer2);
    }
}