package Day3;

import java.util.Arrays;

/**
 * Given a long list of dimensions for hypothetical triangle, identify how many are possible
 * i.e. the longest side is length than the sum of the other two
 */
public class Day3 {

    private int[][] triangleDims;

    private Day3(int[][] triangleDims) {
        this.triangleDims = triangleDims;
    }

    private int countLegitTriangles() {

        int validTraingles = 0;

        for (int[] triangle : triangleDims) {
            // Find the max side length
            int maxVal = triangle[0];
            int[] otherVals = Arrays.copyOfRange(triangle, 1, triangle.length);
            if (otherVals[0] > maxVal) {
                int newMax = otherVals[0];
                otherVals[0] = maxVal;
                maxVal = newMax;
            }
            if (otherVals[1] > maxVal) {
                int newMax = otherVals[1];
                otherVals[1] = maxVal;
                maxVal = newMax;
            }

            if (otherVals[0] + otherVals[1] > maxVal)
                validTraingles++;
        }
        return validTraingles;
    }

    private int countColumnTriangles() {

        int validTriangles = 0;

        int[] tri1 = new int[3];
        int[] tri2 = new int[3];
        int[] tri3 = new int[3];
        int[][] allTris = { tri1, tri2, tri3};

        for (int i = 0; i < triangleDims.length; i++) {
            tri1[i%3] = triangleDims[i][0];
            tri2[i%3] = triangleDims[i][1];
            tri3[i%3] = triangleDims[i][2];

            // If just completed a new set of triangle
            if (i % 3 == 2) {
                for (int[] triangle : allTris) {
                    int maxVal = triangle[0];
                    int[] otherVals = Arrays.copyOfRange(triangle, 1, triangle.length);
                    if (otherVals[0] > maxVal) {
                        int newMax = otherVals[0];
                        otherVals[0] = maxVal;
                        maxVal = newMax;
                    }
                    if (otherVals[1] > maxVal) {
                        int newMax = otherVals[1];
                        otherVals[1] = maxVal;
                        maxVal = newMax;
                    }
                    if (otherVals[0] + otherVals[1] > maxVal)
                        validTriangles++;
                }
            }
        }

        return validTriangles;
    }

    public static void main(String[] args) {
        Data3 input = new Data3();
        int[][] triangleDims = input.triangleDims;
        Day3 today = new Day3(triangleDims);

        // Solve part 1
        int legitTriangles = today.countLegitTriangles();
        System.out.println("Total legit triangles = " + legitTriangles);

        // Solve part 2
        int columnTriangles = today.countColumnTriangles();
        System.out.println("Total column triangles = " + columnTriangles);
    }
}
