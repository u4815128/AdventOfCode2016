package Day4;

import org.junit.Test;

import java.util.*;

/**
 * Finally, you come across an information kiosk with a list of rooms. Of course, the list is encrypted and full of
 * decoy data, but the instructions to decode the list are barely hidden nearby. Better remove the decoy data first.
 *
 * Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID,
 * and a checksum in square brackets.
 *
 * A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order,
 * with ties broken by alphabetization. For example:
 *
 * aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between
 *          x, y, and z, which are listed alphabetically.
 * a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are
 *          listed alphabetically.
 * not-a-real-room-404[oarel] is a real room.
 * totally-real-room-200[decoy] is not.
 *
 * Of the real rooms from the list above, the sum of their sector IDs is 1514.
 *
 * What is the sum of the sector IDs of the real rooms?
 */
public class Day4 {

    private String[] splitInput;
    String[][] processedInput;

    /**
     * Constructor for Day4 class
     * Takes a string array representing individual rooms, and splits it into a matrix of rooms and phrases for
     * the code of each room, separated by hyphens
     * @param splitInput a string array representing individual rooms
     */
    Day4(String[] splitInput) {
        this.splitInput = splitInput;
        processedInput = new String[splitInput.length][];
        for (int i = 0 ; i < splitInput.length; i++) {
            processedInput[i] = splitInput[i].split("-");
        }
    }

    /**
     * Builds a HashMap for given data to count how many times each letter used
     * @param phrases an array containing code for each room, split by hyphens
     * @return a HashMap with counts of letters for the whole code
     */
    private Map<Character, Integer> buildHashMap(String[] phrases) {
        Map<Character, Integer> charCounts = new HashMap<>();
        for (int i = 0; i < phrases.length - 1; i++) {
            for (int j = 0; j < phrases[i].length(); j++) {
                if (charCounts.containsKey(phrases[i].charAt(j))) {         // check if char already added to map
                    int currentVal = charCounts.get(phrases[i].charAt(j));
                    charCounts.put(phrases[i].charAt(j), ++currentVal);
                } else {
                    charCounts.put(phrases[i].charAt(j), 1);
                }
            }
        }
        return charCounts;
    }

    /**
     * Take a generated HashMap of frequency counts and check if the top 5 most frequent letters
     * match the checkSum. If yes, add sector ID to the total count
     * @param processedInput the input data, split into an array of arrays
     * @return an integer indicating the sum of all real rooms
     */
    private int countRealRooms(String[][] processedInput) {
        int sumOfSectorIDs = 0;
        for (int i = 0; i < processedInput.length; i++) {
            // Index of last element in array, which contains the sector ID and checksum
            int len = processedInput[i].length - 1;
            // Extract the sector ID and checkSum
            String hashString = processedInput[i][len];
            int sectorID = Integer.parseInt(hashString.substring(0, 3));
            String checkSum = hashString.substring(4, 9);

            // Convert room code into dictionary to tally uses of each letter
            Map<Character, Integer> charCounts = buildHashMap(processedInput[i]);

            // Look through dictionary, finding char(s) with greatest value(s), then compare with
            // checksum. Continue via while loop until all checked or false obtained
            boolean validRoom = true;
            while (validRoom && checkSum.length() > 0) {
                int biggestVal = 0;
                List<Character> biggestChars = new ArrayList<>();
                for (char character : charCounts.keySet()) {
                    // If larger character, replace list
                    if (charCounts.get(character) > biggestVal) {
                        biggestChars.clear();
                        biggestChars.add(character);
                        biggestVal = charCounts.get(character);
                    // If same size character, add to list for alphabetic sorting
                    } else if (charCounts.get(character) == biggestVal) {
                        biggestChars.add(character);
                    }
                }
                // When finished, sort list alphabetically and check if matches checksum
                Collections.sort(biggestChars);
                for (int j = 0; j < biggestChars.size(); j++) {
                    if (checkSum.length() == 0) {
                        break;
                    } else if (biggestChars.get(j) == checkSum.charAt(0)) {
                        charCounts.remove(checkSum.charAt(0));
                        checkSum = checkSum.substring(1);
                    } else {
                        validRoom = false;
                        break;
                    }
                }
            }
            if (validRoom) {
                sumOfSectorIDs += sectorID;
                checkForHiddenRoom(processedInput[i], sectorID);
            }
        }
        return sumOfSectorIDs;
    }

    private void checkForHiddenRoom(String[] processedInput, int sectorID) {
        int toRotate = sectorID % 26;
        String secretPhrase = "northpole object storage ";
        // Convert processed input back to single string
        String decryptedPhrase = "";
        for (int p = 0 ; p < processedInput.length - 1 ; p++) {
            String phrase = processedInput[p];
            for (int i = 0; i < phrase.length(); i++) {
                int newLetter = phrase.charAt(i) + toRotate;
                if (newLetter > 122)
                    newLetter -= 26;
                decryptedPhrase = decryptedPhrase + (char) newLetter;
            }
            decryptedPhrase = decryptedPhrase + " ";    // add space between words
        }
        if (decryptedPhrase.equals(secretPhrase))
            System.out.println(secretPhrase + "room sector ID: " + sectorID);
    }


    public static void main(String[] args) {
        Data4 rawData = new Data4();
        String[] splitInput = rawData.input.split("\n");
        Day4 today = new Day4(splitInput);
        int roomCount = today.countRealRooms(today.processedInput);
        System.out.println("Final room count: " + roomCount);
    }
}
