package Day1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Noosh on 04/12/16.
 */
public class Day1Test {

    @Test
    public void charToIntTest() {
        assertTrue((int) 'A' == 65);
    }

    @Test
    public void splitTest() {
        String input = "R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5";
        String[] directions = input.split(", ");
        for (int i = 0; i < directions.length; i++) {
            System.out.println(directions[i]);
            System.out.println(directions[i].substring(1));
            System.out.println(2 * Integer.parseInt(directions[i].substring(1)));
        }
    }

    @Test
    public void modTest() {
        System.out.println(0 % 4);
        System.out.println(1 % 4);
        System.out.println(2 % 4);
        System.out.println(3 % 4);
        System.out.println(4 % 4);
    }

    @Test
    public void arrayEqualsTest() {
        int[] a = {1, 2};
        int[] b = {1, 2};
        assertFalse(a.equals(b));
    }
}