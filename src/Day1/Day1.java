package Day1;

import java.util.HashSet;
import java.util.Set;

/**
 * --- Day 1: No Time for a Taxicab ---
 * You're airdropped near Easter Bunny Headquarters in a city somewhere. "Near", unfortunately, is as close as you can
 * get - the instructions on the Easter Bunny Recruiting Document the Elves intercepted start here, and nobody had time
 * to work them out further.
 *
 * The Document indicates that you should start at the given coordinates (where you just landed) and face North.
 * Then, follow the provided sequence: either turn left (L) or right (R) 90 degrees, then walkToEnd forward the given number
 * of blocks, ending at a new intersection.
 *
 * There's no time to follow such ridiculous instructions on foot, though, so you take a moment and work out the
 * destination. Given that you can only walkToEnd on the street grid of the city, how far is the shortest path to the
 * destination?
 *
 * For example:
 * Following R2, L3 leaves you 2 blocks East and 3 blocks North, or 5 blocks away.
 * R2, R2, R2 leaves you 2 blocks due South of your starting position, which is 2 blocks away.
 * R5, L5, R5, R3 leaves you 12 blocks away.
 */

public class Day1 {
    private String[] directions;

    private Day1(String[] directions) {
        this.directions = directions;
    }

    /**
     * Method to follow the directions to the final square
     * @return a Tuple indicating the final x, y position reached
     */
    private int walkToEnd() {

        int directionFacing = 0;    // Takes value 0-3, corresponding to North, East, South, West, respectively
        int x = 0;
        int y = 0;

        for (String entry : directions) {
            // Find new direction facing
            if (entry.charAt(0) == 'L') {
                directionFacing--;
                if (directionFacing < 0) directionFacing = 3;
            } else {
                directionFacing = (directionFacing + 1) % 4;
            }
            int toWalk = Integer.parseInt(entry.substring(1));
            switch (directionFacing) {
                case 0:
                    y += toWalk;
                    break;
                case 1:
                    x += toWalk;
                    break;
                case 2:
                    y -= toWalk;
                    break;
                case 3:
                    x -= toWalk;
                    break;
            }
        }
        return Math.abs(x) + Math.abs(y);
    }

    private int walkToRevisitedSquare() {

        int directionFacing = 0;    // Takes value 0-3, corresponding to North, East, South, West, respectively
        int x = 0;
        int y = 0;
        Set<String> visited = new HashSet<>();

    firstLoop:
        for (String entry : directions) {
            // Find new direction facing
            if (entry.charAt(0) == 'L') {
                directionFacing--;
                if (directionFacing < 0) directionFacing = 3;
            } else {
                directionFacing = (directionFacing + 1) % 4;
            }
            int toWalk = Integer.parseInt(entry.substring(1));

            for (int i = 0; i < toWalk; i++) {
                switch (directionFacing) {
                    case 0:
                        y++;
                        break;
                    case 1:
                        x++;
                        break;
                    case 2:
                        y--;
                        break;
                    case 3:
                        x--;
                        break;
                }
                String newLocation = "(" + x + ", " + y + ")";
                if (!visited.contains(newLocation)) {
                    visited.add(newLocation);
                } else {
                    System.out.println("Repeat square found!");
                    break firstLoop;
                }
            }
        }
        return Math.abs(x) + Math.abs(y);
    }

    public static void main(String[] args) {
        // Split input into an array or directions
        String input = "R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5";
        String[] directions = input.split(", ");
        Day1 today = new Day1(directions);

        // Answer should be 234
        System.out.println("Distance is: " + today.walkToEnd());
        // Answer should get 113
        System.out.println("First square visited twice is: " + today.walkToRevisitedSquare());
    }
}