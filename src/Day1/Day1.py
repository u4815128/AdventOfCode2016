# Day 1 of 2016 Advent of Code
# Identify bunny QH by navigating left or right and walking a given
# number of blocks

# Facing direction takes value 0-3
# 0 = north, 1 = east, 2 = south, 3 = west

directions = 'R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5'#'R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5'
dirs = directions.split(', ')
facingDirection = 0
horizPos = 0
vertPos = 0

def rotate(toRotate):
    global facingDirection
    global horizPos
    global vertPos
    if toRotate == 'L':
        facingDirection -= 1
        if facingDirection < 0:
            facingDirection = 3
    elif toRotate == 'R':
        facingDirection = (facingDirection + 1) % 4
    else:
        print "Error, invalid input encountered"

def walk(steps):
    global facingDirection
    global horizPos
    global vertPos
    if facingDirection == 0:
        vertPos += steps
    elif facingDirection == 1:
        horizPos += steps
    elif facingDirection == 2:
        vertPos -= steps
    elif facingDirection == 3:
        horizPos -= steps
    else:
        print "Error, not a number"
    
# Solution to part 1
def quickWalk():
    for d in dirs:
        # First, adjust direction facing
        rotDirection = d[0]
        rotate(rotDirection)

        # Second, walk
        toWalk = int(d[1:])
        walk(toWalk)

    totalDistance = abs(horizPos) + abs(vertPos)
    return "Total distance walked = " + str(totalDistance)
    # Solution was 253


# Solution to part 2
def slowWalk():
    visited = set()
    for d in dirs:
        # First, adjust direction
        rotDirection = d[0]
        rotate(rotDirection)

        # Then walk, square by square
        toWalk = int(d[1:])
        for step in range(toWalk):
            walk(1)
            currentLoc = (horizPos, vertPos)
            if currentLoc in visited:
                return "HQ is at (" + str(horizPos) + ", " + str(vertPos) + ")"
            else:
                visited.add(currentLoc)

ans1 = quickWalk()
print ans1

facingDirection = 0
horizPos = 0
vertPos = 0

ans2 = slowWalk()
print ans2
